// MtMatrixTest.cpp : Defines the entry point for the console application.
//

#include "MtMatrix.hpp"
#include <random>

//extern template class MtMatrix<int>;
//typedef MtMatrix<int> MtMatrixInt;

void testAcces(const MtMatrix<int>& m)
{
	auto begin = chrono::high_resolution_clock::now();
	int acum = 0;
	for (unsigned short int i = 0; i < m.getRowSize(); i ++)
		for (unsigned short int j = 0; j < m.getColSize(); j ++)
		{
			int a = m.getData(i, j);
			acum = (acum + a )%1000;
		}
	auto end = chrono::high_resolution_clock::now();
	auto finalTime = chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
	cout << acum << endl;
	cout << "Matrix transposed -> "  << m.isTransposed() << endl;
	cout << "AccesTime -> " << double(finalTime) / CLOCKS_PER_SEC  << endl;
}

int main(int argc, char* argv[])
{
	///basic test
//	MtMatrix<int> m1(1,1);
//	cout << m1.getColSize() << " "<< m1.getRowSize() << endl;
//	cout << m1.isOk() << endl;
//	m1.loadIdentity();


	//memory acces transposed test
	//default_random_engine generator;
	//uniform_int_distribution<int> distribution(0,1000);
	//for(int i = 0; i < 100; i++)
	//{
	//	MtMatrix<int> m(100,100);
	//	vector<int> v;
	//	for (auto j = 0; j < 10000; j++){v.push_back(distribution(generator));}
	//	m.loadFromVector(v);
	//	if (i%2) m.transpose();
	//	testAcces(m);
	//}

	////operators test
	//MtMatrix<int> m1(2,2);
	//vector<int> v1;
	//for (auto i = 0; i < 4; i++){v1.push_back(i);}
	//m1.loadFromVector(v1);
	//MtMatrix<int> m2 = m1;
	//m2.scale(10);
	//cout << m1;
	//cout << m2;
	//m2 *= 10;
	//cout << m2;
	//cout << m2*5 + m2;

	//check product with identity
	MtMatrix<int> m1(10,10);
	MtMatrix<int> m2(m1);
	m2.loadIdentity();
	vector<int> v1;
	for (int i = 0; i < 100; i++){v1.push_back(i);}
	m1.loadFromVector(v1);
	m2*=2;
	cout << m1 << endl;
	cout << m2 << endl;
	MtMatrix<int> m3 = (m1*m2);
	cout << m3 << endl;
	//m1.transpose();
	//cout <<  m1 << endl;
	//m3.loadIdentity();
	//cout << m3*m3*2 << endl;

	//check high product
	//MtMatrix<int> m(10,10);
	//vector<int> v;
	//default_random_engine generator;
	//uniform_int_distribution<int> distribution(0,1000);
	//for (auto j = 0; j < 100; j++){v.push_back(distribution(generator));}
	//m.loadFromVector(v);
	//MtMatrix<int> m2 = m;
	//auto begin = chrono::high_resolution_clock::now();
	//m*=m2;
	//cout << m << endl;
	//auto end = chrono::high_resolution_clock::now();
	//auto finalTime = chrono::duration_cast<std::chrono::nanoseconds>(end-begin).count();
	//cout << "Product calculated in-> " << double(finalTime) / CLOCKS_PER_SEC  << endl;
	MtMatrix<int> m4 = {{ 1, 2, 3, 4 },
						{ 5, 6, 7, 8 },
						{ 9, 10, 11, 12 }, 
						{ 13, 14, 15, 16 }};
	cout << m4;
	getchar();
	// test comment added
	return 0;
}