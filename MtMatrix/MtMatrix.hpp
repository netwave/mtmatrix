//__INTERFACE__
#ifndef _MTMATRIX_HPP_
#define _MTMATRIX_HPP_

#include <vector>
#include <thread>
#include <iostream>
#include <initializer_list>
#include "ThreadPooLib/ThreadPool.h"
using namespace std;

template <class Num>
class MtMatrix
{
private:
	unsigned int		m_rowSize;
	unsigned int		m_columnSize;
	Num**				m_matrix;
	bool				m_transposed;
	bool				m_isOk;
	
	
	static  ThreadPool	m_TP;

	void			resize		(unsigned int rowSize, unsigned int colSize);
	bool			valueInVect	(const Num& val, const vector<Num>& v);

public:

	//Constructor, needs matrix size
	//MtMatrix					() = delete;
	MtMatrix					(unsigned int rowSize, unsigned int colSize);
	MtMatrix					(initializer_list<initializer_list<Num>> lst);
	MtMatrix					(const MtMatrix<Num>& copyMatrix);
	~MtMatrix					();

	////Loads
	void				loadIdentity	();
	void				loadFromVector	(const vector<Num>& v);

	////Getters and setters
	unsigned int		getRowSize		() const {return m_rowSize;}
	unsigned int		getColSize		() const {return m_columnSize;}
	bool				isTransposed	() const {return m_transposed;}
	bool				isOk			() const {return m_isOk;}

	Num					getData			(unsigned int row, unsigned int col) const;
	MtMatrix<Num>		getRow			(unsigned int row) const;
	MtMatrix<Num>		getCol			(unsigned int col) const;
	vector<Num>			asVector		() const;

	MtMatrix<Num>		getSubMatrix	(const vector<unsigned int>& delRows, 
										 const vector<unsigned int>& delCols) const;

	MtMatrix<Num>		getSubMatrix	(unsigned int startRow,
										 unsigned int endRow,
										 unsigned int startCol,
										 unsigned int endCol);
	//Set value
	void				setValue		(Num& val,
										 unsigned int row, 
										 unsigned int col);
	////Operations


	////Matrix operations
	void					transpose	();

	virtual MtMatrix<Num>	scale		(Num num);
	virtual MtMatrix<Num>	add			(const MtMatrix<Num>& matrix);
	virtual MtMatrix<Num>	sub			(const MtMatrix<Num>& matrix);
	virtual MtMatrix<Num>	product		(const MtMatrix<Num>& matrix);	
	

	////Self operators
	virtual MtMatrix<Num>&	operator*=	(Num num);
	virtual MtMatrix<Num>&	operator+=	(const MtMatrix<Num>& matrix);
	virtual MtMatrix<Num>&	operator-=	(const MtMatrix<Num>& matrix);
	virtual MtMatrix<Num>&	operator*=	(const MtMatrix<Num>& matrix);
	virtual MtMatrix<Num>&	operator=	(const MtMatrix<Num>& matrix);

	////Boolean operators
	bool					operator==	(MtMatrix<Num> matrix);
	bool					operator!=	(MtMatrix<Num> matrix);

	////Operators
	virtual MtMatrix<Num>	operator*	(Num num);
	virtual MtMatrix<Num>	operator*	(MtMatrix<Num> matrix);
	virtual MtMatrix<Num>	operator+	(MtMatrix<Num> matrix);
	virtual MtMatrix<Num>	operator-	(MtMatrix<Num> matrix);

	template<class Num>
	friend	ostream&		operator<<	(ostream& outS, const MtMatrix<Num>& matrix);


};
//__INTERFACE_END__

template <class Num>
ThreadPool MtMatrix<Num>::m_TP = ThreadPool(ThreadPool::OPTIMUM);

template <class Num>
bool MtMatrix<Num>::valueInVect( const Num& val, const vector<Num>& v )
{
	for (auto& e: v) if(e == val) return true;
	return false;
}


template <class Num>
MtMatrix<Num>::MtMatrix( unsigned int rowSize, unsigned int colSize )
{
	m_rowSize		= rowSize;
	m_columnSize	= colSize;
	m_transposed	= false;
	m_isOk			= false;
	m_matrix		= new Num*[rowSize];
	for (unsigned int i = 0; i < rowSize; i++) m_matrix[i] = new Num[colSize];
}


template <class Num>
MtMatrix<Num>::MtMatrix(initializer_list<initializer_list<Num>> lst)
{
	if (lst.size() > 0)
	{
		m_rowSize		= lst.size();
		m_columnSize	= lst.begin()->size();
		m_transposed	= false;
		m_matrix		= new Num*[m_rowSize];
		for (unsigned int i = 0; i < m_rowSize; i++) m_matrix[i] = new Num[m_columnSize];
		int r = 0;
		int c = 0;
		for (auto& row : lst)
		{
			for (auto& val : row)
			{
				m_matrix[r][c] = val;
				c++;
			}
			c = 0;
			r++;
		}
		m_isOk = true;
	}
	else
	{m_isOk = false;}

}


template <class Num>
MtMatrix<Num>::MtMatrix( const MtMatrix<Num>& copyMatrix )
{
	m_rowSize		= copyMatrix.getRowSize();
	m_columnSize	= copyMatrix.getColSize();
	m_transposed	= false;
	m_matrix		= new Num*[m_rowSize];
	for (unsigned int i = 0; i < m_rowSize; i++) m_matrix[i] = new Num[m_columnSize];
	for (unsigned int r = 0; r < m_rowSize; r++)
		for (unsigned int c = 0; c < m_columnSize; c++)
			m_matrix[r][c] = copyMatrix.getData(r,c);
	m_isOk = true;
}

template <class Num>
MtMatrix<Num>::~MtMatrix()
{
	delete m_matrix;
}


template <class Num>
void MtMatrix<Num>::resize( unsigned int rowSize, unsigned int colSize )
{
	delete	m_matrix;
	m_rowSize		= rowSize;
	m_columnSize	= colSize;
	m_transposed	= false;
	m_isOk			= true;
	m_matrix		= new Num*[rowSize];
	for (unsigned int i = 0; i < rowSize; i++) m_matrix[i] = new Num[colSize];
}

template <class Num>
void MtMatrix<Num>::loadIdentity()
{
	for (unsigned int r = 0; r < m_rowSize; r++)
		for (unsigned int c = 0; c < m_columnSize; c++)
			m_matrix[r][c] = 0;
	if (m_rowSize == m_columnSize)
	{
		for (unsigned int i = 0; i < m_rowSize; i++) m_matrix[i][i] = 1;
		m_isOk = true;
	}
}


template <class Num>
void MtMatrix<Num>::loadFromVector( const vector<Num>& v )
{
	int msize = m_rowSize*m_columnSize;
	if(msize == v.size())
	{
		for (unsigned int r = 0; r < m_rowSize; r++)
			for (unsigned int c = 0; c < m_columnSize; c++)
			{
				m_matrix[r][c] = v[r*m_rowSize+c];
			}
		m_isOk = true;
	}
}


template <class Num>
Num MtMatrix<Num>::getData( unsigned int row, unsigned int col ) const
{
	if (m_isOk)
	{
		if (m_transposed)
		{
			unsigned int tmp = col;
			col = row;
			row = tmp;
		}
		return m_matrix[row][col];
	}
	return Num();
}

template <class Num>
MtMatrix<Num> MtMatrix<Num>::getRow( unsigned int row ) const
{
	MtMatrix<Num>	ret(1,m_columnSize);
	vector<Num>		v;
	for (unsigned int i = 0; i < m_columnSize; i++){v.push_back(getData(row, i));}
	ret.loadFromVector(v);
	return ret;
}


template <class Num>
MtMatrix<Num> MtMatrix<Num>::getCol( unsigned int col ) const
{
	MtMatrix<Num>	ret(1,m_rowSize);
	vector<Num>		v;
	for (unsigned int i = 0; i < m_rowSize; i++){v.push_back(getData(i, col));}
	ret.loadFromVector(v);
	ret.transpose();
	return ret;
}


template <class Num>
vector<Num> MtMatrix<Num>::asVector() const
{
	vector<Num> ret;
	if (m_isOk)
	{
		for (unsigned int r = 0; r < m_rowSize; r++)
			for (unsigned int c = 0; c < m_columnSize; c++)
				ret.push_back(getData(r,c));
	}
	return ret;
}

template <class Num>
MtMatrix<Num> MtMatrix<Num>::getSubMatrix(const vector<unsigned int>& delRows,
										  const vector<unsigned int>& delCols ) const
{
	MtMatrix<Num>	ret(m_rowSize - delRows.size(), m_columnSize - delCols.size());
	vector<Num>		v;
	for (unsigned int r = 0; r < m_rowSize; r++)
		for (unsigned int c = 0; c < m_columnSize; c++)
		{
			if (!valueInVect(r, delRows) && !valueInVect(c, delCols))
			{
				v.push_back(getData(r, c));
			}
		}
	ret.loadFromVector(v);
	return ret;
}


template <class Num>
MtMatrix<Num> MtMatrix<Num>::getSubMatrix(unsigned int startRow, unsigned int endRow, 
										  unsigned int startCol, unsigned int endCol )
{
	if ((startRow < m_rowSize		&& endRow <= m_rowSize) &&
		(startCol < m_columnSize	&& endCol <= m_columnSize))
	{
		MtMatrix<Num>	ret(m_rowSize - delRows.size(), m_columnSize - delCols.size());
		vector<Num>		v;
		for (unsigned int r = startRow; r <= endRow; r++)
			for (unsigned int c = startCol; c <= endCol; c++)
			{
				v.push_back(getData(r, c));
			}
		ret.loadFromVector(v);
		return ret;
	}
	return MtMatrix<Num>;
}


template <class Num>
void MtMatrix<Num>::setValue( Num& val, unsigned int row, unsigned int col )
{
	if (m_isOk)
	{
		if (m_transposed)
		{
			unsigned int tmp = col;
			col = row;
			row = tmp;
		}
		m_matrix[row][col] = val;
	}
}


template <class Num>
void MtMatrix<Num>::transpose()
{
	swap(m_rowSize, m_columnSize);
	m_transposed = m_transposed ? false : true;
}


template <class Num>
MtMatrix<Num> MtMatrix<Num>::scale( Num num )
{
	MtMatrix<Num> ret(*this);
	for (unsigned int r = 0; r < ret.getRowSize(); r++)
		for (unsigned int c = 0; c < ret.getColSize(); c++)
		{
			Num val = ret.getData(r,c)*num;
			ret.setValue(val, r, c);
		}
	return ret;
}


template <class Num>
MtMatrix<Num> MtMatrix<Num>::add( const MtMatrix<Num>& matrix )
{
	MtMatrix<Num> ret(*this);
	for (unsigned int r = 0; r < m_rowSize; r++)
		for (unsigned int c = 0; c < m_columnSize; c++)
		{
			Num val = ret.getData(r,c) + matrix.getData(r,c);
			ret.setValue(val, r, c);
		}
	return ret;
}


template <class Num>
MtMatrix<Num> MtMatrix<Num>::sub( const MtMatrix<Num>& matrix )
{
	MtMatrix<Num> ret(*this);
	for (unsigned int r = 0; r < m_rowSize; r++)
		for (unsigned int c = 0; c < m_columnSize; c++)
		{
			Num val = ret.getData(r,c) - matrix.getData(r,c);
			ret.setValue(val, r, c);
		}
		return ret;
}

template <class Num>
MtMatrix<Num> MtMatrix<Num>::product( const MtMatrix<Num>& matrix )
{
	
	if (getColSize() == matrix.getRowSize())
	{
		MtMatrix<Num> ret(getRowSize(), matrix.getColSize());
		ret.loadIdentity();
		vector<thread*> tv;
		for (unsigned int r = 0; r < ret.getRowSize(); r++)
			for (unsigned int c = 0; c < ret.getColSize(); c++)
			{
				auto f = [=,&ret,&matrix,this]()
				{
					vector<Num> row = getRow(r).asVector();
					vector<Num> col = matrix.getCol(c).asVector();
					Num productValue = 0;
					for (unsigned int i = 0; i < row.size(); i ++)
					{
						Num tmp = row[i]*col[i];
						productValue += tmp;
					}
					ret.setValue(productValue, r, c);
				};
				thread* t = m_TP.getNewThread();
				*t = thread(f);
				tv.push_back(t);
			}
		for (auto& t : tv)	if (t->joinable()) t->join();
		return ret;
	}
	return MtMatrix<Num>(0,0);
}


template <class Num>
MtMatrix<Num>& MtMatrix<Num>::operator*=( Num num )
{
	*this = scale(num);
	return *this;
}


template <class Num>
MtMatrix<Num>& MtMatrix<Num>::operator+=( const MtMatrix<Num>& matrix )
{
	*this = add(matrix);
	return *this;
}


template <class Num>
MtMatrix<Num>& MtMatrix<Num>::operator-=( const MtMatrix<Num>& matrix )
{
	*this = sub(matrix);
	return *this;
}


template <class Num>
MtMatrix<Num>& MtMatrix<Num>::operator*=( const MtMatrix<Num>& matrix )
{
	*this = product(matrix);
	return *this;
}


template <class Num>
MtMatrix<Num>& MtMatrix<Num>::operator=(const  MtMatrix<Num>& matrix )
{
	m_rowSize		= matrix.getRowSize();
	m_columnSize	= matrix.getColSize();
	m_transposed	= false;
	m_matrix		= new Num*[m_rowSize];
	for (unsigned int i = 0; i < m_rowSize; i++) m_matrix[i] = new Num[m_columnSize];
	for (unsigned int r = 0; r < m_rowSize; r++)
		for (unsigned int c = 0; c < m_columnSize; c++)
			m_matrix[r][c] = matrix.getData(r,c);
	m_isOk = true;
	return *this;
}


template <class Num>
bool MtMatrix<Num>::operator==( MtMatrix<Num> matrix )
{
	if ((m_rowSize		== matrix.getRowSize()) && 
		(m_columnSize	== matrix.getColSize()))
	{
		for (unsigned int r = 0; r < m_rowSize; r++)
			for (unsigned int c = 0; c < m_columnSize; c++)
				if(getData(r, c) != matrix.getData(r,c))
					return false;
		return true;
	}
	return false;
}


template <class Num>
bool MtMatrix<Num>::operator!=( MtMatrix<Num> matrix )
{
	
	return !(*this == matrix);
}

template <class Num>
MtMatrix<Num> MtMatrix<Num>::operator*( Num num )
{
	return scale(num);
}


template <class Num>
MtMatrix<Num> MtMatrix<Num>::operator*( MtMatrix<Num> matrix )
{
	return product(matrix);
}


template <class Num>
MtMatrix<Num> MtMatrix<Num>::operator+( MtMatrix<Num> matrix )
{
	return add(matrix);
}


template <class Num>
MtMatrix<Num> MtMatrix<Num>::operator-( MtMatrix<Num> matrix )
{
	return sub(matrix);
}


template <class Num>
ostream& operator<<( ostream& outS, const MtMatrix<Num>& matrix )
{
	for (unsigned int r = 0; r < matrix.getRowSize(); r++)
	{
		outS << "| ";
		for (unsigned int c = 0; c < matrix.getColSize(); c++)
		{
			outS << matrix.getData(r,c) << " ";
		}
		outS<< "|" << endl;
	}
	return outS;
}
#endif