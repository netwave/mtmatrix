#ifndef	_THREAD_POOL_
#define _THREAD_POOL_

#include <vector>
#include <thread>

using namespace std;


class ThreadPool
{

public:
	enum Mode
	{
		LOCKED,		//Just 1 thread
		FIXED,		//Custom defined max threads
		OPTIMUM,	//Optimum number of threads, 1 peer core
		UNLOCKED	//Infinite number of threads
	};

private:

	vector<thread*>	m_pool;
	Mode			m_mode;
	unsigned int	m_maxThreads;

public:

	ThreadPool(Mode mode = OPTIMUM, unsigned int numberOfThreads = 1);
	~ThreadPool();

	thread* getNewThread();
	int		getMaxThreads();
	int		getTotalThreads();
	int		getActiveThreads();
};

#endif